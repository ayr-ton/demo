resource "cloudflare_record" "ayr-ton_link" {
    zone_id = var.hosts.zone_ids.records.ayr-ton_link
    name    = "@"
    value   = var.hosts.homeserver.records.aaaa
    type    = "AAAA"
    proxied = true
}
