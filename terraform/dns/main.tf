provider "cloudflare" {
  version = "~> 2.0"
  email = var.email
  api_key = var.token
}

module "domains" {
  source = "./domains"
  hosts = var.hosts
}
