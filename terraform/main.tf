terraform {
  backend "pg" {}
}

module "dns" {
  source = "./dns"
  hosts  = var.hosts

  token = var.cloudflare_token
  email = var.cloudflare_email
}

