variable "cloudflare_email" {
}

variable "cloudflare_token" {
}

variable "hosts" {
  default = {
    home_server = {
      records = {
        "a"          = "some IPv4"
        "aaaa"       = "some IPv6"
      }
    }
    zone_ids = {
      records = {
        "ayr-ton_link" = "cloudflare zone_id"
      }
    }
  }
}

